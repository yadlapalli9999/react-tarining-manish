import React from 'react';

function List() {
  const [thing, setThing] = React.useState('');
  const [things, setThings] = React.useState([]);
  const [editItem, setEditItem] = React.useState(null);
  /*
    [
        {
            id: 1,
            thing: 'movies',
            uid: 1
        },
        {
            id: 2,
            thing: 'cricket',
            uid: 1
        }
    ]
  */

  const handleSubmit = (e) => {
    e.preventDefault();
    const obj = {
      id: Date.now().toString(),
      thing: thing,
      uid: 1
    };
    setThings([...things, obj]);
    setThing('');
  };

  const handleDelete = (id) => {
    if (!things) return;
    if (things.length === 0) return;
    const index = things.findIndex((elem) => {
      return elem.id === id;
    });

    const copyOfThings = [...things];
    copyOfThings.splice(index, 1); // delete process
    setThings(copyOfThings);
  };

  const handleEdit = (e) => {
    e.preventDefault();
    if (!things) return;
    if (things.length === 0) return;

    const index = things.findIndex((elem) => {
      return elem.id === editItem.id;
    });

    const obj = {
      ...editItem,
      thing: thing
    };
    const copyOfThings = [...things];
    copyOfThings.splice(index, 1, obj);
    setThings(copyOfThings);
    setEditItem(null);
    setThing('');
  };

  return (
    <div className="container" style={{ marginTop: '20px' }}>
      <h3>Things I like</h3>
      {/*Add Form*/}
      {!editItem && (
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="things">Things</label>
            <input
              type="text"
              className="form-control"
              id="things"
              aria-describedby="emailHelp"
              placeholder="Add things which you like?"
              value={thing}
              onChange={(e) => {
                setThing(e.target.value);
              }}
            />
            <small id="thingsHelp" className="form-text text-muted">
              We'll never share your email with anyone else.
            </small>
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      )}

      {/*Edit Form*/}
      {editItem && (
        <form onSubmit={handleEdit}>
          <div className="form-group">
            <label htmlFor="things">Edit Things</label>
            <input
              type="text"
              className="form-control"
              id="things"
              aria-describedby="emailHelp"
              placeholder="Add things which you like?"
              value={thing || editItem.thing}
              onChange={(e) => {
                setThing(e.target.value);
              }}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Update
          </button>
        </form>
      )}

      {/*List to users*/}
      {things && things.length > 0 && (
        <table className="table" style={{ marginTop: '20px' }}>
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Things I Like</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {things.map((item) => {
              return (
                <tr key={item.id}>
                  <th scope="row">{item.id}</th>
                  <td>{item.thing}</td>
                  <td>
                    <span
                      onClick={() => {
                        setEditItem(item);
                      }}
                      style={{ cursor: 'pointer' }}>
                      Edit
                    </span>
                  </td>
                  <td>
                    <span
                      onClick={() => {
                        handleDelete(item.id);
                      }}
                      style={{ cursor: 'pointer' }}>
                      Delete
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default List;
