import React from 'react';
import Home2 from './Home2';

const data = [
  {
    id: 1,
    name: 'John',
    age: 34
  },
  {
    id: 2,
    name: 'Bob',
    age: 23
  },
  {
    id: 3,
    name: 'Doe',
    age: 35
  }
];

function Home() {
  const [counter, setCounter] = React.useState(0);
  const [title, setTitle] = React.useState('Yahoo');
  console.log('counter is ', counter);

  const handleCounter = () => {
    setCounter(counter + 1);
  };

  const handleTitle = (val) => {
    setTitle(val);
  };

  return (
    <div>
      <h1>Hello world</h1>
      <h3>Title is {title}</h3>
      <button onClick={handleCounter}>Handle Me</button>
      <div>Counter value is {counter}</div>
      {data && data.length > 0 ? (
        data.map((item) => {
          return (
            <Home2
              key={item.id}
              name={item.name}
              age={item.age}
              handleTitle={handleTitle}
            />
          );
        })
      ) : (
        <div>No Data found</div>
      )}
    </div>
  );
}

export default Home;
