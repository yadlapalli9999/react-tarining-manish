import React from 'react';
import { Form } from 'react-bootstrap';

const Password = (props) => {
  return (
    <Form.Group controlId={props.name}>
      <Form.Label>{props.title}</Form.Label>
      <Form.Control
        type="password"
        placeholder={props.placeholder}
        name={props.name}
        onChange={props.handleChange}
      />
    </Form.Group>
  );
};

export default Password;
