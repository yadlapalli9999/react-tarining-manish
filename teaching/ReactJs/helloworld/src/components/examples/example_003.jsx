import React from 'react';

const Example = () => {
  const [obj, setObj] = React.useState({ x: 0, y: 0 });

  const func1 = (event) => {
    console.log('x: ', event.pageX, ', y: ', event.pageY);
    setObj({ x: event.pageX, y: event.pageY });
  };
  React.useEffect(() => {
    window.addEventListener('mousemove', func1);

    return () => {
      console.log('done');
      window.removeEventListener('mousemove', func1);
    };
  }, []);
  return (
    <div>
      Mouse position: {obj.x}, {obj.y}
    </div>
  );
};

export default Example;
