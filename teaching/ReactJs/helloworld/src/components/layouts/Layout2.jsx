import React from 'react';
import Nav from '../pages/Nav';

const Layout2 = (props) => {
  return (
    <div className="container">
      <h1>Layout 2 Page</h1>
      <Nav />
      <hr />
      {props.children}
      <hr />
      <div>This is footer.</div>
    </div>
  );
};

export default Layout2;
