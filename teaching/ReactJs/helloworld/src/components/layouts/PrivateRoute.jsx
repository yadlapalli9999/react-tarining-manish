import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = (props) => {
  const {
    component: Component,
    isAuthenticated,
    layout: Layout,
    location,
    ...rest
  } = props;
  return (
    <Route
      {...rest}
      render={(matchingProps) => {
        if (isAuthenticated) {
          return (
            <Layout>
              <Component {...matchingProps} />
            </Layout>
          );
        } else {
          return (
            <Redirect to={{ pathname: '/login', state: { from: location } }} />
          );
        }
      }}
    />
  );
};

export default PrivateRoute;
