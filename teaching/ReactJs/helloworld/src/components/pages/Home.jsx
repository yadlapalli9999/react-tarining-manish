import React from 'react';
import { Link, Route } from 'react-router-dom';
import Home2 from './Home2';
const Home = () => {
  return (
    <div>
      <Link to="/home2">Home Page</Link>
      <Route component={Home2} path="/home2" />
    </div>
  );
};

export default Home;
