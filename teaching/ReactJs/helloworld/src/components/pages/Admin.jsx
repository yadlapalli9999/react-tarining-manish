import React from 'react';

function Admin() {
  return <div>Admin page, only logged in users are allowed</div>;
}

export default Admin;
