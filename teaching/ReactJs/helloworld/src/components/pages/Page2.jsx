import React from 'react';
import Page3 from './Page3';

function Page2(props) {
  console.log(props);
  return (
    <div>
      Page 2
      <Page3 {...props} var1="watermelon" />
    </div>
  );
}

export default Page2;
