import React from 'react';
import { Button } from 'react-bootstrap';

const Page = (props) => {
  return (
    <div>
      <h3>{props.title}</h3>
      <div>{props.description}</div>
      <div>{props.name}</div>
      <Button
        variant="primary"
        onClick={() => {
          props.setName(props.title);
        }}>
        {props.buttonTitle}
      </Button>
    </div>
  );
};

export default Page;
