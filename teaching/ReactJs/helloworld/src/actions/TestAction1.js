export const action1 = (val) => {
  return async (dispatch, getState) => {
      dispatch({
          type: 'ADD',
          payload: val
      });
  };
};

export const action2 = (val) => {
  return async (dispatch, getState) => {
    dispatch({
        type: 'SUBTRACT',
        payload: val
    });
  };
};
