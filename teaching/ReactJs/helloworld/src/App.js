import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Page1 from './components/pages/Page1';
import Admin from './components/pages/Admin';
import Example from './components/examples/example_005';

import MainLayout from './components/layouts/MainLayout';
import Layout2 from './components/layouts/Layout2';
import LayoutRoute from './components/layouts/LayoutRoute';

import PrivateRoute from './components/layouts/PrivateRoute';

import ToDo from './components/ToDo/ToDo';
import Parent from './components/examples/Parent';

function App() {
  return (
    <Router>
      <Switch>
        <LayoutRoute
          component={Example}
          path="/example"
          exact
          layout={MainLayout}
        />
        <LayoutRoute component={ToDo} path="/todo" exact layout={MainLayout} />
        <LayoutRoute
          component={Example}
          path="/example/:id"
          exact
          layout={MainLayout}
        />
        <LayoutRoute
          component={Parent}
          path="/parent"
          exact
          layout={MainLayout}
        />
        <PrivateRoute
          component={Admin}
          path="/admin"
          exact
          layout={MainLayout}
          isAuthenticated={false}
        />
        <Route
          path="/login"
          exact
          render={(matchingProps) => {
            console.log('matchingProps: ', matchingProps);
            return <div>Login Please</div>;
          }}
        />
        <LayoutRoute
          component={About}
          path="/about"
          exact
          layout={MainLayout}
        />
        <LayoutRoute
          component={Contact}
          path="/contact"
          exact
          layout={Layout2}
        />
        <Route
          path="/test"
          exact
          render={(matchingProps) => {
            return <div>Hello World</div>;
          }}
        />
        <Route
          path="/test2"
          exact
          render={(matchingProps) => {
            return <About {...matchingProps} />;
          }}
        />
        <Route
          path="/page1"
          exact
          render={(matchingProps) => {
            return <Page1 {...matchingProps} />;
          }}
        />
        <LayoutRoute component={Home} path="/" layout={MainLayout} />
      </Switch>
    </Router>
  );
}

export default App;
