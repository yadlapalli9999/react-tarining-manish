const initialState = {
  data3: null,
  data4: null,
  result: 0
};

const TestReducer2 = (state = initialState, action = {}) => {
  switch (action.type) {
    case 'INCREMENT':
      state = {
        ...state,
        result: state.result + 1
      };
      break;
    case 'DECREMENT':
      state = {
        ...state,
        result: state.result - 1
      };
      break;
    case 'DATA3':
      state = { ...state, data3: action.payload };
      break;
    case 'DATA4':
      state = { ...state, data4: action.payload };
      break;
    default:
      break;
  }

  return state;
};

export default TestReducer2;
