//First convert number to string
//Convert string to Array
//Reverse the array
//join method to convert array to string
//ParseInt method to convert string back to number
//Math.sign(num) * result

function reverseInt(num) {
  // given an integer, it will reverse it
  // 24 => 42
  // 100 => 1
  // -50 => -5
  // [2, 4] => [4, 2] => 42 => Math.sign(num) * 42
  return (
    Math.sign(num) * parseInt(num.toString().split('').reverse().join(''), 10)
  );
}
reverseInt(24);
//42
reverseInt(100);
//1
reverseInt(-500);
//-5
