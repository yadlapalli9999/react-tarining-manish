var friends = [
  {
    name: 'Anna',
    books: ['Bible', 'Harry Potter'],
    age: 21
  },
  {
    name: 'Bob',
    books: ['War and peace', 'Romeo and Juliet'],
    age: 26
  },
  {
    name: 'Alice',
    books: ['The Lord of the Rings', 'The Shining'],
    age: 18
  }
];

// all books - list which will contain all friends books
function listOfBooks(arr) {
  return arr.reduce((acc, cur) => {
    return [...acc, ...cur.books];
  }, []);
}

var allBooks = listOfBooks(friends);
console.log(allBooks);
// output: ["Bible", "Harry Potter", "War and peace", "Romeo and Juliet", "The Lord of the Rings", "The Shining"]
