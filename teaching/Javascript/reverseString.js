function reverseString(str) {
  // Step 1: Convert the string to Array. cat, ['c', 'a', 't']
  const arr = str.split('');
  // Step 2: Array, reverse, ['t', 'a', 'c']
  const reverseArr = arr.reverse();
  // Step 3: Convert array to string and return it.
  const finalStr = reverseArr.join('');

  return finalStr;
}

reverseString('cat');
// "tac"
