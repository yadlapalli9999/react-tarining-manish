// We have a string, return the most commonly character in the string.
// maxchar('abbc') output: b
// maxchar("hello world") output: l

function maxchar(str) {
  if (!str) return null;
  const obj = {};
  let max = 0;
  let maxChar = '';

  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    if (obj[char]) {
      //exist
      obj[char]++;
    } else {
      // does not exist
      obj[char] = 1;
    }

    if (obj[char] > max) {
      max = obj[char];
      maxChar = char;
    }
  }
  return maxChar;
}
maxchar('abbc');
// b
maxchar('hello world');
// l
