function func1() {
  var i,
    res = 0;
  var number_of_params = arguments.length;
  for (i = 0; i < number_of_params; i++) {
    res = res + arguments[i];
  }
  return res;
}

func1(1, 2, 3);
// output 6
