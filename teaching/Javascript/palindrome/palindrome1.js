//Palindrome
// Palindromes are strings that form the same word if it is reversed.
// cat => tac - this is not a palindrome
// abba => abba - this is palindrome
// abcba => abcba - this is palindrome

function palindrome(str) {
  const reversed = str.split('').reverse().join('');
  return reversed === str;
}
palindrome('abba');
// true

palindrome('cat');
// false
