
Day 18 (15th October, 2020)
React, PropTypes and Default Props
OO JS,
Problems.










=======================
Live Project,

Front end React/Redux,
Backend will be ParseServer -> Mongodb

Compass db, check 

Node js with mysql, mongodb, etc.


Day 17 (14th October, 2020)
OoJs,
Problems JS,
React Redux.

Problems:
Palindrome.

OO JS:
Scope and Hoisting.

Functions are data:
function f() {return 1;}
var f = function() {
    return 1;
}
The second way of defining a function is known as literal function.

1. They contain code.
2. They are executable (can be invoked). 
()

Function can also be treated as a normal variable, it can be copied to different variable and even deleted.

var sum = function(a, b) { return a + b;}
var add = sum;
delete sum;
typeof sum;
// undefined
typeof add;
"function"
add(1,2);
//3

we cannot start the function name with a number , characters, numbers, underscore.

Anonymous Function: 
its ok to have pieces of data lying around progam.
"test"; [1,2,3]; undefined; null; 1;

functions are like any other variable, so they can also be used without beign assigned a name.


function(a) { return a;}
function f(a, b, B) {
    var sum = a + b;
    B(sum);
}

f(1, 2, function(a) { return a;})

View todo  items, 
Delete todo items.

Day 16 (13th October, 2020),
Oo JS,
Problems,
React Redux,

Scope of Variables
function scope, if a variable is defined inside a function, its not visible outside of the function.
Global variables. The term global variables describes variables you define outside of any function, as opposed to local variables which are defined inside a function.
The code inside a function has access to all global variables as well as to its own local variables.

var global = 1;
function f() {
    var local = 2;
    global++;
    return global;
}
f(); // 2
f(); // 3
local; // undefined

If you don't use var to declare a variable, this variable is assigned global scope.
function f2() {
    local2 = 2;
}

f2();
local2; // 2

var x = 1;
x = x + 2;
var a = 123;

// Javascript interprets it like:
var x;
var a;
x = 1;
x = x + 2;
a = 123;



function f() {
    alert(a); // undefined
    var a = 1;
    alert(a); // 1
}

function f() {
    var a;
    alert(a);
    a = 1;
    alert(a);

}

f();

var a = 123;
function() {
    var a = 0;
    a++;

}
alert(a);


Concept of Hoisting. Where variables and function declarations are moved to the top of the their scope before code execution.

js file at bottom of html i.e. before the body ends 

Reverse integer problem.

Redux:


Day 15 (12th October, 2020)
React + Redux, 
Problems,
Oo JS.

React Redux Integration:
Step 1: Install following packages
npm install --save redux react-redux redux-devtools-extension redux-thunk redux-logger

Step 2: We need to create store.js file and call this store.js in index.js file.
done
Step 3: Create Reducer File.
done
Step 4: Create Action File.
done
Step 5: Create a file called as rootReducer and include all reducers in it.
done
Step 6: Call action and reducer in each component
Done

From next monday, we will start our real project, 1000 project.

Day 14 (9th October, 2020)
Problems in Javascript
Oo Js.
React.

Redux, 
We can build whole website without using redux.
const [user, setUser] = React.useState(null);
few pages in my react app, 

1000 of pages: one page username, another page username, 
global state.

Day 13 (8th October, 2020)
1. React, Ajax request, and
2. OoJ, function, and
3. Few Problems on Reduce.

Day 12 (6th October, 2020)

Events, States and useEffect
Events

function handleChange() { // Input is changed

}

function handleSubmit() { // when form is submitted

}

function handleClick() { // we use this function

}

<button onclick="handleClick()">Submit</button>

In jsx,
<button onClick={handleClick}>Submit</button>

State (Hooks in React)
function App() {
    const [counter, setCounter] = React.useState(0);
    const [name, setName] = React.useState('John');
    return (<div>
    <button onClick={() => {
        setCounter(counter + 1);
        setName('Bob');
    }}>Submit</button>
    </div>);
}

function App2() {
    // username
    const [counter, setCounter] = React.useState(0);
    const [name, setName] = React.useState('John');
    return (<div>
    <button onClick={() => {
        setCounter(counter + 1);
        setName('Bob');
    }}>Submit</button>
    </div>);
}

Redux 

useEffect

function App() {

    return 
}

Day 11 (5th October, 2020)
Lists and Keys
Lists or display of items in components.

Reduce in javascript and Functions in Javascript

React, Problems, OOJ, NodeJS
Basics, Building , 1000 project, small, 6 months to 1 year.

Day 10 (2nd October, 2020)
Routing - Private Routing
Javascript Problem,

Private Routing:
1. Step 1: Passing Authenticated Variable. (true or false)

2. Step 2: PrivateRoute:
if user is auth: <Component />
if user is not auth: Redirect to user to login page.

3. Step 3: App.js
PrivateRoute, auth flag, 

Problem:
//Cat
function reverseString(str) {
    // create a new variable
    let newStr = '';
    // loop through str variable and put each character before the previour character
    for (let i = 0; i < str.length; i++) {
        newStr = str[i] + newStr;
        // 'c'
        // 'ac'
        // 'tac'
    }
    // Returning my new string with reverse of str
    return newStr;
}
Redux, Concept of Object Oriented Javascript, Problems, Node Js

Day 9 (1st Oct, 2020)
Routes,
Javascript (Problems) 



Layout Route:
Step 1: Create 2 or more layout components like MainLayout, Layout2.
Step 2: Create LayoutRoute component, which will display other layouts.
Step 3. In app.js,
import Mainlayout, Layout2, and LayoutRoute.
Step 4: Create Route using LayoutRoute instead of Route.
<LayoutRoute path="/" exact component={Home} layout={MainLayout} />
<LayoutRoute path="/" exact component={Home} layout={Layout2} />


Today's Problem:
Reverse the String:
For example: apple, you should return elppa,
cat, => tac

Method 1:

some input and some output



function reverseString(str) {
    // Step 1: Convert the string to Array. cat, ['c', 'a', 't'] 
    const arr = str.split('');
    // Step 2: Array, reverse, ['t', 'a', 'c']
    const reverseArr = arr.reverse();
    // Step 3: Convert array to string and return it.
    const finalStr = reverseArr.join('');

	return finalStr;
}


Day 8 (29th Sep, 2020)
Routes
Component Children Property.
Passing Props to other component.

Day 7 (28th sep, 2020),
Destructuring,
React Router - to create different urls 
app
    about,
    home,
    contact
Install a package 'react-router-dom';
npm install react-router-dom


Day 6 (26th sep, 2020),
npm install react-select
Destructuring (basic)

Day 5 (25th sep, 2020),
Textarea, react-quill

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

https://www.npmjs.com/package/react-quill


Day 4 (24th sep, 2020),
Build a form and other components of Bootstrap

Day 3 (23rd sep, 2020)
Props and States in React JS and how to use them.

How to pass Props.
How to access Props.
How to create States.
How to update the States.
How to pass state as props.

Day 2 (22st sep, 2020)
npm install react-bootstrap bootstrap

Open index.js file and add following statement
import 'bootstrap/dist/css/bootstrap.min.css';

We have to create different components based on the template.
To create component, we have to use following code:

import React from 'react';

const ComponentName = () => {
    return (<div>Hello World</div>);
};

export default ComponentName;

We can call this component in another File or another component as:

import ComponentName from './ComponentNameFile.js';

To convert html to jsx, use following website:
https://magic.reactjs.net/htmltojsx.htm


Day 1 (21st sep, 2020)

I will be teaching following subjects:

Javascript
Object Oriented Javascript
Interview questions in Javascript
300 to 500 javascript problems related to interview.
Data Structure and Algorithms

React JS
Node JS

Mysql db
Firebase db
Parse Server db

Editor
https://code.visualstudio.com/

Git
https://git-scm.com/downloads

Git Tortoise
https://tortoisegit.org/

Node JS
https://nodejs.org/en/download/

Firebase
https://console.firebase.google.com/u/0/


React Js
npx create-react-app helloworld



Crud Operations: Backend database
Add form
Edit form
View page
Detail page
Delete page

Javascript, Swap the variable.

Node JS
Firebase, Node JS (Mysql Database)
